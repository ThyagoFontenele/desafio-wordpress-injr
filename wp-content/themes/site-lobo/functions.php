<?php

function alura_intercambios_registrando_taxonomia(){
    register_taxonomy(
        'lobos',
        'customposts',
        array(
            'labels' => array('name' => 'Lobos'),
            'hierarchical' => true
        )
    );
}
add_action('init','alura_intercambios_registrando_taxonomia');

function RegistrandoPosts(){
    register_post_type('customposts',
        array(
            'labels' => array('name' => 'Custom Posts'),
            'public' => true,
            'menu-position' => 0,
            'supports' => array('title', 'editor', 'thumbnail'),
            'menu_icon' => 'dashicons-admin-site'
        )
    );
}
add_action('init', 'RegistrandoPosts');

function AdicionandoRecursos(){
    add_theme_support('post-thumbnails');
}
add_action('after_setup_theme', 'AdicionandoRecursos');
function registrando_menu(){
    register_nav_menu(
        location: 'menu-navegacao',
        description: 'Menu navegacão'
    );
}
add_action('init', 'registrando_menu');