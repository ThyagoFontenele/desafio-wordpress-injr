<?php
require_once 'header.php';
$args = array('post-type' => 'customposts');
$query = new WP_Query($args);
if($query ->have_posts()){
    while($query -> have_posts()){
        $query->the_post();
        the_post_thumbnail();
        the_title();
        the_content();
    }
}

require_once 'footer.php';
