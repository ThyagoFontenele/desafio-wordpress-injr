<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'desafio-wordpress' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         ',JM9z}f{Q[wwa?3$]u]$@dxw}I9fo_]s0|OlG+wadoXop6If-uS[E8GFWZ:A5-ZI' );
define( 'SECURE_AUTH_KEY',  '&HJGUg6r8+k*<Q,>VMQsSBym~YRyah>%o,k5e4vu[20..3f6rH0gV}v4(S+NOBEY' );
define( 'LOGGED_IN_KEY',    '9e`ymiP&OmW)aMNwot97skNONi]xJP-a@ae~Q$2(zrWa24cwmoNt*&QzFnf]jB+*' );
define( 'NONCE_KEY',        'QHz43Tigpgj]O+uWsBNThej4{s(WZ7awHc_c-x!m^y/X+{Y2<&`6>8]AtkD&,pNI' );
define( 'AUTH_SALT',        'e.FiijM$g~-(n[wx_xs;0&NNVO]aI8eV6+I:gad|Tt&>!Ikw<8uz~F_-%^+c:^|H' );
define( 'SECURE_AUTH_SALT', '3E#o,HI_cpbXG%eF6gHs 4kKZ?xh+{5wh5y(,G[dJ?CZiA])Mb~gZGUwfT*Glh<t' );
define( 'LOGGED_IN_SALT',   'q-4IECN@PhK]0Ns-]};$p#j1$y[&~MXtmED$84h V<qS0D!+FkQ*WE+IHF=H(Ej1' );
define( 'NONCE_SALT',       'WQn<t.Xv)v)%v[{OW N{wZva6vLA{l~It&E4We$v,:`^6Oa.@P=/a^5TCMZHJH<G' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
